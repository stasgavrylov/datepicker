// Requiring packages
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoPrefixer = require('gulp-autoprefixer'),
    minifyCSS= require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    minifyHTML = require('gulp-minify-html'),
    imageMin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    jsHint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    cache = require('gulp-cache'),
    del = require('del'),
    gulpFilter = require('gulp-filter'),
    mainBowerFiles = require('main-bower-files'),
    connect = require('gulp-connect'),
    gulpIf = require('gulp-if'),
    opn = require('opn');

// Declaring paths and variables
var src = {
        js: ['./src/js/**/*.js'],
        sass: ['./src/sass/main.css'],
        images: ['./src/img/**/*.*'],
        index: ['./src/index.html']
    },

    server = {
        host: 'localhost',
        port: '9000'
    },

    env,
    outputDir,
    sassStyle,
    sassComments;

// Configuring paths and options for different environments
env = process.env.NODE_ENV || 'dev';

if (env === 'dev') {
    outputDir = 'builds/development/';
    sassStyle = 'expanded';
    sassComments = true;
} else {
    outputDir = 'builds/production/';
    sassStyle = 'compressed';
    sassComments = false;
}





/**********
 * ~TASKS~
 **********/

// Start webserver
gulp.task('webServer', function() {
    connect.server({
        root: outputDir,
        host: server.host,
        port: server.port,
        livereload: true
    });
});

// Open browser
gulp.task('openBrowser', function() {
    opn( 'http://' + server.host + ':' + server.port);
});

// ~ Clean ~
// Delete build folders
gulp.task('cleanDev', function() {
    del(['./builds/development'], function (err, deletedFiles) {
        console.log('Files deleted:', deletedFiles.join(', '));
    });
});

gulp.task('cleanProd', function() {
    del(['./builds/production'], function (err, deletedFiles) {
        console.log('Files deleted:', deletedFiles.join(', '));
    });
});

gulp.task('clean', function() {
    del(['./builds'], function (err, deletedFiles) {
        console.log('Files deleted:', deletedFiles.join(', '));
    });
});





// ~ Compile styles ~
var cssFilter = gulpFilter('*.css');

// Concat vendor CSS (uglify for production)
gulp.task('styles:vendor', function() {
  gulp.src(mainBowerFiles())
  .pipe(cssFilter)
  .pipe(concat('vendor.css'))
  .pipe(gulpIf(env !== 'dev', minifyCSS()))
  .pipe(gulp.dest(outputDir + 'css'))
});

// Concat own SASS (uglify for production)
gulp.task('styles', function() {
    gulp.src(src.sass)
    // .pipe(sass({
    //         "sourcemap=none": true,
    //         noCache: true,
    //         compass: true,
    //         style: sassStyle,
    //         lineNumbers: sassComments
    //     }))
    .pipe(autoPrefixer())   
    .pipe(gulp.dest(outputDir + 'css'))
    .pipe(connect.reload())
});





// ~ Compile JS ~
var jsFilter = gulpFilter('*.js');

// Concat vendor JS (uglify for production)
gulp.task('js:vendor', function() {
  gulp.src(mainBowerFiles())
      .pipe(jsFilter)
      .pipe(concat('vendor.js'))
      .pipe(gulpIf(env !== 'dev', uglify()))
      .pipe(gulp.dest(outputDir + 'js'))
});

// Concat own JS (uglify for production)
gulp.task('js', function() {
    gulp.src(src.js)
        .pipe(jsHint())
        .pipe(jsHint.reporter('default'))
        .pipe(concat('script.js'))
        .pipe(gulpIf(env !== 'dev', uglify()))
        .pipe(gulp.dest(outputDir + 'js'))
        .pipe(connect.reload());
});

// Compress images and move 'em to output dir
gulp.task('images', function() {
    return gulp.src(src.images)
        .pipe(imageMin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(outputDir + 'img'))
        .pipe(connect.reload())
});



// Copy index to output dir (minify for production)
gulp.task('index', function() {
    gulp.src(src.index)
        .pipe(gulpIf(env !== 'dev', minifyHTML()))
        .pipe(gulp.dest(outputDir))
        .pipe(connect.reload())
});







// Watch for changes in /src directories
gulp.task('watch', function() {
    gulp.watch(src.js, ['js']);
    gulp.watch('./src/sass/*.css', ['styles']);
    gulp.watch(src.index, ['index']);
    gulp.watch(src.images, ['images']);
});



// ~Build tasks~
//Build dev version
gulp.task('build', ['styles:vendor', 'styles', 'js:vendor', 'js', 'images', 'index']);

// Build and run dev environment
gulp.task('default', ['build', 'webServer', 'openBrowser', 'watch']);