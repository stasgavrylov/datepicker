(function() {

    // Object with general information.
    var calendarData = {
        days: ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'],
        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        monthNamesFull: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        monthsLength: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    };

    // Compile calendar element.
    function createCustomElement(el, elClass, elID) {
        var newEl = document.createElement(el);
        newEl.className = elClass;
        newEl.id = elID || '';
        return newEl;
    }

    var calendarDiv = createCustomElement('div', 'calendar', 'calendar'),
        navDiv = createCustomElement('div', 'nav'),
        prevArrow = createCustomElement('a', 'prevScreen', 'prevScreen'),
        header = createCustomElement('a', 'calendarHeader', 'calendarHeader'),
        nextArrow = createCustomElement('a', 'nextScreen', 'nextScreen'),
        daysDiv = createCustomElement('div', 'days'),
        calendarTable = createCustomElement('table', 'calendarTable', 'calendarTable'),
        todayBtn = createCustomElement('button', 'todayBtn', 'todayBtn');
    
        navDiv.appendChild(prevArrow);
        navDiv.appendChild(header);
        navDiv.appendChild(nextArrow);

        daysDiv.appendChild(calendarTable);

        todayBtn.textContent = 'Today';

        calendarDiv.appendChild(navDiv);
        calendarDiv.appendChild(daysDiv);
        calendarDiv.appendChild(todayBtn);

        document.body.appendChild(calendarDiv);

    // Add logic for arrows.
    prevArrow.addEventListener('click', switchBackward);
    nextArrow.addEventListener('click', switchForward);

    // Pop-up datepicker on input focus.
    var inputs = document.getElementsByTagName('input');

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].getAttribute('type') == "date") {
            var dateInput = inputs[i];
            (function(_dateInput) {
                _dateInput.addEventListener('focus', function() {
                    var hasValue = Boolean(_dateInput.value),
                        dateArray;
                    // To recall datepicker with picked date.
                    if (hasValue) {
                        var year = Number(_dateInput.value.slice(0,4)),
                            month = Number(_dateInput.value.slice(5,7)) - 1,
                            date = Number(_dateInput.value.slice(8));
 
                        dateArray = [year, month, date];
                    }
                    initDatepicker(dateArray, _dateInput);
                });
            })(dateInput);
        }  // if
    } // for
    

    // Handle clicks outside of datepicker.
    document.addEventListener('click', function(e) {
        var clickedDateInput = e.target.getAttribute('type') == "date";

        if (!clickedDateInput) hideDatepicker();    
    });

    calendarDiv.addEventListener('click', function(e) {
        e.stopPropagation();
    });

    // Datepicker initialization.
    function initDatepicker(initDate, inputField) { 

        // Get current date.
        var today = new Date(),
            year = today.getFullYear(),
            month = today.getMonth(),
            date = today.getDate();

        // Save it to global object for further checks.
        calendarData.today = [year, month, date],
        calendarData.activeInput = inputField;

        // Set default Year view header limits.
        // This is a crutch for the next 15 years :)
        if (year < 2016) {
            setHeaderLimits(2000, 2015);
            calendarData.lowerLimit = 2000;
            calendarData.upperLimit = 2015;
        } else {
            setHeaderLimits(2016, 2030);
            calendarData.lowerLimit = 2016;
            calendarData.upperLimit = 2030;
        }

        // If input doesn't have data yet, render table for today's date.
        // Else render table for received input value.
        (!initDate) ? fillDaysTable(year, month, date) : fillDaysTable(initDate[0], initDate[1], initDate[2]);
        
        // Display datepicker and position it below the input.
        var inputCoords = inputField.getBoundingClientRect();

        calendarDiv.style.left = inputCoords.left + 'px';
        calendarDiv.style.top = inputCoords.top + inputField.offsetHeight + 5 + 'px';
        calendarDiv.style.display = 'block';

        // Set 'Today' button.
        todayBtn = clearElementListeners(todayBtn);

        todayBtn.addEventListener('click', function() {
            fillDaysTable(year, month, date);
            setHeaderLimits(calendarData.lowerLimit, calendarData.upperLimit);
            setDate(year, month + 1, date, inputField);
            hideDatepicker();
        });
    } // initDatepicker()

    // Fill Days View.
    function fillDaysTable(year, month, date) {
        clearTable();
        header = clearElementListeners(header);

        // If leap year, change February to 29 days.
        calendarData.monthsLength[1] = (year % 4 === 0) ? 29 : 28;

        // Get info about first day of the month.
        var firstDayOfMonth = new Date(year, month, 1),
            firstDayIndex = firstDayOfMonth.getDay(),
            monthLength = calendarData.monthsLength[month],
            
        // Prepare data for calendar table.
            previousMonth = (month === 0) ? 11 : month - 1,
            currentMonthFlag = (firstDayIndex === 0) ? true : false,
            dayCounter = 0,
            monthData = previousMonth + 1, // adding +1, because input takes [1-12] month values
            yearData = (previousMonth === 11 && firstDayIndex !== 0) ? year - 1 : year; // for cases when January doesn't start on Sunday

        // If no date submitted, mark 1st day of month as activeDay.
        if (!date) date = 1;

        // If first day of month is not Sunday,
        // we set dayCounter to start count from previous month days.
        if (firstDayIndex > 0) {
            dayCounter = calendarData.monthsLength[previousMonth] - firstDayIndex + 1;
        } else {
            dayCounter = 1;
            // Adjust month data to start from current month.
            monthData++;
        }

        // Fill calendar header.
        header.textContent = calendarData.monthNamesFull[month] + ' ' + year;
        header.setAttribute('data-role', 'month');
        header.setAttribute('data-month', month);
        header.setAttribute('data-year', year);

        // Prepare DocumentFragment to fill it with table content.
        var tableFragment = document.createDocumentFragment();

        // Fill table with generated data.
        for (var i = 0; i < 7; i++) {
            var row = document.createElement('tr');
            for (var j = 0; j < 7; j++) {

                var dayInRow = document.createElement('td'),
                    dayLink = document.createElement('a');

                // If first row, fill with weekday names.
                if (i === 0) { 
                    dayInRow.innerHTML = calendarData.days[j];
                    dayInRow.className = "weekDay";
                }
                else {
                    // Reset dayCounter to 1 on beginning of current month.
                    if (firstDayIndex > 0 && 
                            dayCounter > calendarData.monthsLength[previousMonth] && i === 1) {
                        dayCounter = 1;
                        currentMonthFlag = true;
                        monthData++;
                        if (yearData < year) yearData = year; // switch to current year if necessary
                    }

                    // Check if we're still withing limits.
                    if (monthData > 12) monthData = 1; // switch to January                    
                    
                    // Mark today.
                    if (currentMonthFlag && date == dayCounter) dayInRow.className = "activeDay ";

                    // Reset dayCounter to 1 on beginning of next month.
                    if ((i > 4) && (dayCounter > monthLength)) {
                        dayCounter = 1;
                        currentMonthFlag = false;
                        monthData = (month === 11) ? 1 : ++monthData; // switch to January if necessary
                        yearData =  (month === 11) ? year + 1 : year; // switch to next year in December
                    }

                    // Mark other months gray.
                    if (!currentMonthFlag) dayInRow.className = "notCurrentMonth ";

                    dayLink.textContent = dayCounter;
                    dayLink.href = "#";
                    dayLink.setAttribute('data-month', monthData);
                    dayLink.setAttribute('data-year', yearData);
                    dayInRow.appendChild(dayLink);
                    dayCounter++;

                    dayInRow.className = dayInRow.className + 'calendarDay';
                }
                row.appendChild(dayInRow);
            }
            tableFragment.appendChild(row);
        } // for cycle

        // Append created table content to the table
        calendarTable.appendChild(tableFragment);

        // Make header switch to Months View
        header.addEventListener('click', function() {
            fillMonthsTable(Number(header.dataset.year), Number(header.dataset.month));
        });

        // Make day links set the input date value.
        // And mark clicked day link.
        var dayLinks = document.querySelectorAll('.calendarDay a');
        for (var i = 0; i < dayLinks.length; i++) {
            dayLinks[i].addEventListener('click', function(e) {

                var targetLink = e.target,
                    activeDay = document.querySelector('.activeDay'),
                    activeDayClasses = activeDay.className;

                activeDayClasses = activeDayClasses.split('activeDay ').join('');
                activeDay.className = activeDayClasses;
                targetLink.parentElement.className = 'activeDay ' + targetLink.parentElement.className;


                var year = targetLink.dataset.year,
                    month = targetLink.dataset.month,
                    date = targetLink.textContent;
                setDate(year, month, date, calendarData.activeInput);

                hideDatepicker();
            });
        }
    } // fillDaysTable()

    // Fill Months View.
    function fillMonthsTable(year, month) {

        clearTable();  
        header = clearElementListeners(header);

        // Fill header.
        header.textContent = year;
        header.setAttribute('data-role', 'year');
        header.setAttribute('data-year', year);
        header.setAttribute('data-month', month);

        calendarTable.className = "calendarTable monthTable";

        // Prepare DocumentFragment to fill it with table content.
        var tableFragment = document.createDocumentFragment(), 
            monthCounter = 0,
            currentMonth = Number(month),
            currentYear = Number(year);

        // Fill fragment with months.
        for (var i = 0; i < 4; i++) {

            var row = document.createElement('tr');

            for (var j = 0; j < 3; j++) {

                var monthInRow = document.createElement('td'),
                    monthLink = document.createElement('a');
                    monthLink.href = '#';
                    monthLink.setAttribute('data-month', monthCounter);

                // Mark current month.
                if (monthCounter === currentMonth) {
                    monthInRow.className = "activeMonth ";
                }

                monthLink.textContent = calendarData.monthNamesShort[monthCounter];
                monthCounter++;

                monthInRow.className = monthInRow.className + 'monthCell';
                monthInRow.appendChild(monthLink);
                row.appendChild(monthInRow);
            }
            tableFragment.appendChild(row);
        } // for cycle

        calendarTable.appendChild(tableFragment);

        // Make month links switch to corresponding days view.
        var monthLinks = document.querySelectorAll('.monthCell a');
        for (var i = 0; i < monthLinks.length; i++) {
            monthLinks[i].addEventListener('click', function() {
                fillDaysTable(Number(header.dataset.year), Number(this.dataset.month));
            });
        }

        // Make header switch to Years View
        header.addEventListener('click', function() {
            var lowerLimit = Number(header.dataset.lowerlimit),
                upperLimit = Number(header.dataset.upperlimit),
                year = Number(header.dataset.year);

            fillYearsTable(lowerLimit, upperLimit, year);
        });
    } // fillMonthsTable()

    // Fill Years View.
    function fillYearsTable(lowerLimit, upperLimit, year) {
        clearTable();

        calendarTable.className = "calendarTable yearTable";

        // Set header.
        header.textContent = lowerLimit + ' – ' + upperLimit;
        header.setAttribute('data-role', 'decade');
        header.setAttribute('data-lowerLimit', lowerLimit);
        header.setAttribute('data-upperLimit', upperLimit);
        
        // Prepare DocumentFragment to fill it with table content.
        var tableFragment = document.createDocumentFragment(), 
            // Set counter and fill years table.
            yearCounter = lowerLimit;

        for (var i = 0; i < 4; i++) {
            var row = document.createElement('tr');

            for (var j = 0; j < 4; j++) {

                var yearInRow = document.createElement('td'),
                    yearLink = document.createElement('a');
                    yearLink.href = '#';
                    yearLink.setAttribute('data-year', yearCounter);

                // Mark current month.
                if (yearCounter === calendarData.today[0]) {
                    yearInRow.className = "activeMonth ";
                }
 
                yearLink.textContent = yearCounter;
                yearCounter++;

                yearInRow.className = yearInRow.className + 'yearCell';
                yearInRow.appendChild(yearLink);
                row.appendChild(yearInRow);
            }
            tableFragment.appendChild(row);
        } // for cycle

        calendarTable.appendChild(tableFragment);

        // Add logic to year links.
        var yearLinks = document.querySelectorAll('.yearCell a');
        for (var i = 0; i < yearLinks.length; i++) {
            yearLinks[i].addEventListener('click', function(e) {
                fillMonthsTable(Number(e.target.dataset.year), Number(header.dataset.month));
            });
        }
    } //fillYearsTable()

    // Clear table from child elements.
    function clearTable() {
        while (calendarTable.firstChild) {
            calendarTable.removeChild(calendarTable.firstChild);
        }
    }

    // To remove all header's event listeners we clone header element
    // and return its clone.
    function clearElementListeners(oldElement) {
        var newElement = oldElement.cloneNode(true);

        oldElement.parentNode.replaceChild(newElement, oldElement);
        return newElement;
    }

    // Logic for 'prev' arrow.
    function switchBackward() {

        var month = Number(header.dataset.month),
            year = Number(header.dataset.year),
            lowerLimit = Number(header.dataset.lowerlimit),
            upperLimit = Number(header.dataset.upperlimit);

        if (header.dataset.role === "month") {
            
            var isLastMonth = month === 0,
                monthToRender = isLastMonth ? 11 : month - 1,
                yearToRender = isLastMonth ? year - 1 : year;

            fillDaysTable(yearToRender, monthToRender);
            // Adjust year limits if we pass one.
            if (isLastMonth && year === lowerLimit) {
                setHeaderLimits(lowerLimit - 15, upperLimit - 15);
            }
        }

        if (header.dataset.role === "year") {
            if (year === lowerLimit) {
                setHeaderLimits(lowerLimit - 15, upperLimit - 15);
            }
            fillMonthsTable(year - 1, month);
        }

        if (header.dataset.role === "decade") {
            fillYearsTable(lowerLimit - 15, upperLimit - 15);
        }
    }

    // Logic for 'next' arrow.
    function switchForward() {

        var month = Number(header.dataset.month),
            year = Number(header.dataset.year),
            lowerLimit = Number(header.dataset.lowerlimit),
            upperLimit = Number(header.dataset.upperlimit);

        if (header.dataset.role === "month") {

            var isLastMonth = month === 11,
                monthToRender = isLastMonth ? 0 : month + 1,
                yearToRender = isLastMonth ? year + 1 : year;

            fillDaysTable(yearToRender, monthToRender);
            if (isLastMonth && year === upperLimit) {
                setHeaderLimits(lowerLimit + 15, upperLimit + 15);
            }
        }

        if (header.dataset.role === "year") {
            if (year === upperLimit) {
                setHeaderLimits(lowerLimit + 15, upperLimit + 15);
            }
            fillMonthsTable(year + 1, month);
        }

        if (header.dataset.role === "decade") {
            fillYearsTable(lowerLimit + 15, upperLimit + 15);
        }
    }

    // Set header year limits.
    function setHeaderLimits(lowerLimit, upperLimit) {
        header.setAttribute('data-lowerLimit', lowerLimit);
        header.setAttribute('data-upperLimit', upperLimit);
    }

    // Set picked date in the input field.
    function setDate(year, month, date, inputField) {
        // Format data
        var month = ('00' + month).slice(-2),
            date = ('00' + date).slice(-2);

        inputField.value = year + '-' + month + '-' + date;
    }

    // Hide datepicker.
    function hideDatepicker() {
            calendarDiv.style.display = 'none';
    }

}()); 